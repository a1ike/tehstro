$(document).ready(function () {

  $('.phone').inputmask('+7(999)999-99-99');

  $('.t-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.t-header__row').slideToggle('fast');
    $('.t-header__nav').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.t-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.t-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.t-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.t-top').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    vertical: true,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false
      }
    }]
  });

  $('.t-home-clients__cards').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    vertical: true,
    verticalSwiping: true,
    swipeToSlide: true,
    autoplaySpeed: 2000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });

  $('.t-home-papers__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    prevArrow: $('.t-home-papers__arrow_prev'),
    nextArrow: $('.t-home-papers__arrow_next'),
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });

  $('.t-product__big').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.t-article__gallery-cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });

  $('ul.tabs li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  $('.t-contacts__cities li').click(function (e) {
    e.preventDefault();

    newLocation(parseInt($(this).attr('data-map-lat'), 10), parseInt($(this).attr('data-map-lng'), 10));

    var tab_id = $(this).attr('data-tab');

    $('.t-contacts__cities li').removeClass('t-contacts__cities_active');
    $('.t-contacts__content-tab').removeClass('active');

    $(this).addClass('t-contacts__cities_active');
    $('#' + tab_id).addClass('active');
  });
});
